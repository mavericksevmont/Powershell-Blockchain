<#
    .Synopsis
       Simple blockchain in powershell
    .DESCRIPTION
       Can add blocks to the blockchain with any string data in the data field
       Can also verify the integrity of the blockchain to prevent fake or corrupted blocks
       
       Available functions:
       generate-nextblock -data
       hash -textToHash
       get-genesisBlock
       calculate-hash -index -previousHash -timestamp -data
       get-latestBlock
       generate-nextBlock -blockdata
       check-sameBlock -block1 -block2
       check-validNewBlock -newBlock -previousBlock
       check-validChain -blockchainToValidate

       Available variables
       $blockchain - array with blocks

    .NOTES
        Author:  Jack Swedjemark         
        Updated: 13/12/2017
        PSVer:   5.0     
    #>

# the block class, storing necessary data
class Block
{
    # instance variables for class
    [int]$index
    [string]$previousHash
    [DateTime]$timestamp
    [string]$data
    [string]$currentHash

    # constructor for class
    Block([int]$index, [string]$previousHash, [DateTime]$timestamp, [string]$data, [string]$currentHash)
    {
        $this.index = $index
        $this.previousHash = $previousHash
        $this.timestamp = $timestamp
        $this.data = $data
        $this.currentHash = $currentHash
    }
}

# function to hash a string using sha256
function Hash($textToHash)
{
    $hasher = new-object System.Security.Cryptography.SHA256Managed
    $toHash = [System.Text.Encoding]::UTF8.GetBytes($textToHash)
    $hashByteArray = $hasher.ComputeHash($toHash)
    foreach($byte in $hashByteArray)
    {
         $res += $byte.ToString()
    }
    return $res;
}

# function to generate the first block in the blockchain
function get-GenesisBlock()
{
    return [Block]::new(0, '0', (get-date 01/01/2010), 'First block', (Hash('Genesis')))
}

# function to calculate the hash of an object
function calculate-hash($index, $previousHash, $timestamp, $data)
{
    $value = [string]$index + [string]$previousHash + [string]$timestamp + [string]$data
    $sha = hash($value)
    return [string]$sha
}

# function taking a block and calulating the hash of it
# using the calculate-hash function
function calculate-hashForBlock([Block]$block)
{
    return calculate-hash($block.index, $block.previousHash, $block.timestamp, $block.data)
}

# function to get the latest block in the chain
function get-latestBlock()
{
    return $blockchain[$blockchain.Length -1]
}

# function to generate the next block
function generate-nextBlock($blockData)
{
    [Block]$previousBlock = get-latestBlock
    [int]$nextIndex = $previousBlock.index + 1
    [DateTime]$nextTimestamp = get-date
    [string]$nextHash = calculate-hash($nextIndex, $previousBlock.currentHash, $nextTimestamp, $blockData)
    return [Block]::new($nextIndex, $previousBlock.currentHash, $nextTimestamp,$blockData, $nextHash)
}

# function to check if 2 blocks are the same
function check-sameBlock($block1, $block2)
{
    if ($block1.index -ne $block2.index)
        {return $false;write-verbose "index mistmatch"}
    elseif ($block1.previousHash -ne $block2.previousHash)
        {return $false;write-verbose "previous hash mistmatch"}
    elseif ($block1.timestamp -ne $block2.timestamp)
        {return $false;write-verbose "timestamp  mistmatch"}
    elseif ($block1.data -ne $block2.data)
        {return $false;write-verbose "data mistmatch"}
    elseif ($block1.currentHash -ne $block2.currentHash)
        {return $false;write-verbose "current hash mistmatch"}
    write-verbose "Blocks match"
    return $true
}

# function to check if a new block is valid
function check-validNewBlock($newBlock, $previousBlock)
{
    if ($previousBlock.index +1 -ne $newBlock.index)
        {
        write-host "Indexes does not match"
        return $false
        }
    elseif ($previousBlock.currentHash -ne $newBlock.previousHash)
        {
        write-host "Previous hash does not match"
        return $false
        }
    elseif ((calculate-hashForBlock $newBlock) -ne $newBlock.currentHash)
        {
        write-host "Hash invalid"
        return $false
        }
    return $true
}

# function to check if entier blockchain is valid
function check-validChain($blockChainToValidate)
    {
    #write-host "First block is $($blockChainToValidate[0].data)"
    #write-host "Content of temp var is $blockChainToValidate"
    # check if first block is the genesis block
#    check-sameBlock $blockChainToValidate[0] get-GenesisBlock
    if (-Not (check-sameBlock ($blockChainToValidate[0]) (get-GenesisBlock)))
        {
        # if not, chain is not valid
        write-host "Genesis block incorrect"
        return $false
        }
    # array to store each block temporarily
    $tempBlocks = @($blockChainToValidate[0])
    # iterate each block in blockchain
    for ($i=1;$i -lt $blockChainToValidate.length; $i ++)
        {
        # check if current block is valid based on previous block
        if (check-validNewBlock $blockChainToValidate[$i] $tempBlocks[$i -1])
            {
                # if it is, add to temporary blockchain array
                $tempBlocks += $blockChainToValidate[$i]
            }
        # if one block does not match, chain is invalid
        else
            {return $false}
        # if all blocks match, chain is valid
        return $true
        }
    }

# create the blockchain by generating the genesis block and adding it to an array
$blockchain = @(get-GenesisBlock)

# generate a bunch of blocks
for ($i = 1; $i -lt 100; $i++)
{
    $blockchain += generate-nextBlock("data$i")
    write-host "Adding block $i to blockchain"
}

# check if blockchain is valid
write-host "Validity of the blockchain is: $(check-validChain $blockchain)"

# user instructions
write-host ""
write-host 'Use $blockchain += generate-nextblock "somedata" to generate new blocks and add them to the blockchain' -ForegroundColor Yellow
write-host 'and check-validchain $blockchain to validate the chain is still correct' -ForegroundColor Yellow
write-host ""

# some function tests

# testing of function calculate-hashforblock
# calculate-hashForBlock $blockchain[0]

# testing of function calculate-hash
# calculate-hash($blockchain[0].index, $blockchain[0].previousHash, $blockchain[0].timestamp, $blockchain[0].data)